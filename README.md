HiQontrol
=========

Soundraft Si Compact 16 Harman HiQnet Controller

Status
======

Work In Progress, [Proof of Concept](https://github.com/EMATech/HiQontrol/releases/tag/v0.0.1) available for Android

Dependencies
============

- Python 2 or 3
- [Kivy](kivy.org)
- netifaces
- twisted

License
=======
Copyright (C) 2014-2015 Raphaël Doursenaud <rdoursenaud@free.fr>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
